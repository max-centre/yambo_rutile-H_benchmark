# How to install Yambo code

Here the instruction on how to compile anche install the [Yambo code](http://www.yambo-code.org/).

## Requirements

The Yambo code needs at least a suit of compilers that contains a Fortran compiler and C/C++ compiler. In order to compile the code with the MPI support it is required also an MPI implementation.

In order to download the code the Linux commands `wget` or `git` can be used. In addition, they may be necessary during the compilation of the external libraries (optional).

## Download

You can download a tarball of a GPL release from the [git repository](https://github.com/yambo-code/yambo/wiki/Releases-(tar.gz-format)):
```
wget https://github.com/yambo-code/yambo/archive/5.0.4.tar.gz -O yambo-5.0.4.tar.gz
```
Or you can clone the develop git repository:
```
git clone git@github.com:yambo-code/yambo-devel.git
```
but for this you need privileged access.

## Quick configuration (with long compilation)

Moving in the source directory you can configure quickly Yambo and start the compilation simply with these two commands:
```
./configure
make core
```
This leads to a very long compilation because Yambo will download and compile all the required libraries (IOTK, HDF5, NETCDF, LIBXC, LAPACK).

In order to prevent to compile all the libraries at every new compilation it is possible to use the configure option `--with-extlibs-path=<path>` to install the external libraries in the specitied `<path>` and reuse them later with the same option.

## Custom configuration

It is possible to customize the configurazion using configure options that allow you to enable or disable some features. In order to have a complete list of all those options use the command
```
./configure --help
```
The suggested options are:
```
--enable-mpi              # already enabled by default
--enable-open-mp
```
to enable MPI and OpenMP parallelization strategies;
```
--enable-time-profile
--enable-memory-profile
```
to enable time and memory profiling (very useful for benchmarking and debugging);
```
--enable-par-linalg
--enable-slepc-linalg
```
to enable rispectively the support to the parallel (with ScaLAPACK) linear algebra and the suport for the diagonalization of BSE using SLEPc library (these two options are mutually exclusive);
```
--enable-cuda=<opts>
```
to enable CUDA support.

### About external libraries

Usually in HPC systems you can find already installed the library needed by Yambo. So you can use them through the specific configure options. Here below some examples.

In order to link to a specific installation of a library you can use the relative configure option and specify the path:
```
--with-hdf5-path=</path/to/hdf5>
--with-netcdf-path=</path/to/netcdf-c>
--with-netcdff-path=</path/to/netcdf-fortran>
```
Here and example on how to use an installation of the Intel MKL libraries for the linear algebra and the Fourier transformations:
```
--with-blas-libs="-L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl" \
--with-lapack-libs="-L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl" \
--with-scalapack-libs="-L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64" \
--with-blacs-libs="-L${MKLROOT}/lib/intel64 -lmkl_blacs_intelmpi_lp64" \
--with-fft-includedir="${MKLROOT}/include" \
--with-fft-libs="-L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl"
```

## Installation

It is not really necessary to install Yambo. At the end of the compilation you will find the executables in the `bin` sub-folder of the source directory. You can use them direcly or copy in a different folder.

## Benchmarks configuration

### Marconi-SKL @ CINECA
[Marconi UserGuide](https://wiki.u-gov.it/confluence/display/SCAIUS/UG3.1%3A+MARCONI+UserGuide)

```
module purge
module load profile/candidate
module load autoload intelmpi/2020--binary
module load mkl/2020--binary

export FC=ifort
export F77=ifort
export CC=icc 
export F90SUFFIX=".f90"
export MPIFC=mpiifort
export MPIF77=mpiifort
export MPICC=mpiicc
export MPICXX=mpiicpc
export FCFLAGS="-assume bscc -O3 -g -ip -xCOMMON-AVX512 -qopt-zmm-usage=high"

./configure \
    --enable-mpi --enable-open-mp \
    --enable-msgs-comps \
    --enable-time-profile \
    --enable-memory-profile \
    --with-blas-libs="-L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl" \
    --with-lapack-libs="-L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl" \
    --with-scalapack-libs="-L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64" \
    --with-blacs-libs="-L${MKLROOT}/lib/intel64 -lmkl_blacs_intelmpi_lp64" \
    --with-fft-includedir="${MKLROOT}/include" \
    --with-fft-libs="-L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl" \
    --with-extlibs-path=${HOME}/opt/ext-libs-intel2020

make core
```

### Marconi100 @ CINECA
[Marconi100 UserGuide](https://wiki.u-gov.it/confluence/display/SCAIUS/UG3.2%3A+MARCONI100+UserGuide)
```
module purge
module load hpc-sdk/2021--binary
module load spectrum_mpi/10.4.0--binary
module load cuda/11.0

export FC=pgf90 
export F77=pgfortran
export CPP='cpp -E' 
export CC=pgcc 
export FPP="pgfortran -Mpreprocess -E"
export F90SUFFIX=".f90"
export MPIFC=mpipgifort
export MPIF77=mpipgifort
export MPICC=mpipgicc
export MPICXX=mpipgic++

./configure \
    --enable-cuda=cuda11.0,cc70 \
    --enable-mpi --enable-open-mp \
    --enable-msgs-comps \
    --enable-time-profile \
    --enable-memory-profile \
    --enable-par-linalg \
    --with-blas-libs="-lblas" \
    --with-lapack-libs="-llapack" \
    --with-extlibs-path=${HOME}/opt/ext-libs-nvhpc21-spectrum10.4

make core
```
