# How to execute the rutile-H benchmark with Yambo

## Preparation

- Clone the development git repository of Yambo and compile the code. See the `INSTALL.md` file.

- Clone this repository with the `SAVE` directory containing the databases obtained by a `DFT` calculation and converted and initialized for Yambo.

- Create a directory for the benchmark named using the following code:
  ```
  <branch_name>-v<version>-rev<revision_num>-hash<hash_num>-results-at-<cluster_name>
  ```
  where `<branch_name>` is the name of the branch of the development repository used for the compilation. You can find the other labels `<version>`, `<revision_nm>`, and `<hash_num>` in the header part of the help message of Yambo (typing "`yambo -h`"). Eventially add a label that points out modifications at the input file.

- Move into the benchmark folder and create a symbolic link to the `SAVE` directory. This folder contains only files used in input so it can be shared between many calculation or benchmark directories.

- Copy here the `template_yambo_rutile.in` file and use it to create many input files as the runs that compose your banchmark. Name them `yambo_rutile_<ntasks>mpi.in`, where `<ntasks>` is the number of MPI tasks used for the run related to that input file.

- For every input file edit those three lines
  ```
  X_and_IO_CPU= "1 1 1 40 4"
  DIP_CPU= "4 20 2"
  SE_CPU= "1 4 40"
  ```
  in a way that the product of the numbers in a line have to be equal to the number of the mpi tasks of the run. These three variables are used to tune the mpi parallelization strategy. Every line is related to a different driver/portion of the simulation and the numbers indicates how many mpi tasks are assigned to a specific level of the parallelization. Some levels of parallelization are useful for obtain better performance, others for a better distribution of the memory. So the way you choose how to distribute the mpi tasks on the differrent levels of parallelism can drastically change the performance of your simulation. The order of the parallelization levels/roles are defined with these three lines (I suggest to not modify them):

  ```
  X_and_IO_ROLEs= "q g k c v"
  DIP_ROLEs= "k c v"
  SE_ROLEs= "q qp b"
  ```

- Use the template at the following session to create a batch script to launch the jobs of the benchmark. The template was written to be used on a CINECA's cluster that uses the `slurm` scheduler, so edit it in order to obtain a valid batch script for your cluster.

## Job submission (batch script template)

```
#!/bin/bash
#SBATCH --nodes=40
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks-per-socket=2
#SBATCH --cpus-per-task=32
#SBATCH --gres=gpu:4
#SBATCH --partition=m100_usr_prod
#SBATCH --time=2:00:00
#SBATCH --account=Pra20_MaX
#SBATCH --mem=230000MB
#SBATCH --job-name=rutile
#SBATCH --error=err.job-%j
#SBATCH --output=out.job-%j
#SBATCH --mail-type=ALL
#SBATCH --mail-user=nicola.spallanzani@nano.cnr.it

module purge
module load hpc-sdk/2021--binary
module load spectrum_mpi/10.4.0--binary
module load cuda/11.0

EXT_LIBS=${HOME}/opt/ext-libs-nvhpc21-spectrum10.4
YAMBO_BIN=${HOME}/src/yambo-devel/bin
YAMBO_LIB=${EXT_LIBS}/pgi/mpipgifort/lib
YAMBO_LIBV4=${EXT_LIBS}/pgi/mpipgifort/v4/serial/lib

export LD_LIBRARY_PATH=$YAMBO_LIB:$YAMBO_LIBV4:$LD_LIBRARY_PATH
export PATH=$YAMBO_BIN:$PATH
export OMP_NUM_THREADS=8

JOB_LABEL=rutile_${SLURM_NTASKS}mpi

mpirun --map-by socket:PE=8 --rank-by core \
       -np ${SLURM_NTASKS} \
       ${YAMBO_BIN}/yambo -F yambo_${JOB_LABEL}.in -J ${JOB_LABEL}.out
```

## Extract the data

For every run that complete correctly it will be produced a report file named `r-${LOB_LABEL}.out_HF_and_locXC_gw0_rim_cut_em1d_ppa` that in tail reports the time spend by many drivers/portions of the code that are considered relevant.

We created a Jupyter notebook that can be used to extract the timing data for every report file. So, copy the file `template_scaling.ipynb` in the benchmark folder and run all the cells of the notebook. Eventually edit the arguments passed to the funcions `build_scaling_plot` and `build_cost_plot` in the last two cells in order to obtain the desired plots.

The call of the function `build_scaling_plot` creates a figure (on screen and on a file in PNG format) with a bar plot for the benckmark. By defaults the bars are created piling the calculation times of these portions of the run: `Dipoles`, `Xo`, `X`, `Self energy`, `Other`. The overall height of the bar is the total time (in seconds) spent by the run. In the same figure is added a line plot indicating the relative efficiency of the runs.

Calling the function `build_cost_plot` an additional figure will be created with a plot that reports the cost (in terms of nodes*hours) of the runs against the time to solution.
